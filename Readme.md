# i3-wm_personalConfig
My personal configurations file to use i3 tiling window manager. See my tutorial pressing [here](https://jenazad.github.io/window_manager/Installing-i3-Tiling-Window-Manager).  
Here you can found my configuration files:

* `i3status.conf` file copy to `/etc/i3status.conf`.

* `config` file copy to `/etc/i3/config`

* `images` directory, move to `~/.i3/images/`.

* To install, you should write these lines in shell:

        sudo -i
        echo "deb http://debian.sur5r.net/i3/ $(lsb_release -c -s) universe" >> /etc/apt/sources.list
        apt-get update
        apt-get --allow-unauthenticated install sur5r-keyring
        apt-get update
        apt-get install i3

* Choose your $mod key, i choose windows key, here is a list of functions:

       $mod + Enter        Open terminal
        $mod + A            Focus to "parent"
        $mod + S            Set Stacked (Cascada)
        $mod + W            Set Tabbed
        $mod + E            Set default
        $mod + SpaceBar     Change focus tiling/floating
        $mod + D            dmenu
        $mod + H            Split Horizontal
        $mod + V            Split Vertical
        $mod + J            Foco izquierda
        $mod + K            Focus down
        $mod + L            Focus up
        $mod + ñ            Focus right
        $mod + Shift + Q    Close window
        $mod + Shift + E    Exit to i3
        $mod + Shift + C    Recarcar configuración sin reiniciar
        $mod + Shift + R    Restart i3bar
        $mod + Shift + J    Move window to left
        $mod + Shift + :    Move window to right
        $mod + Shift + K    Move window to down
        $mod + Shift + L    Move window to up
        $mod + Shift + SpaceBar   Change focus tiling/floating
        $mod + Shift + x    Lock Screen

